﻿//eVision-Show Image Sample (ptr version)
//by Jos Lim (joslimzy@gmail.com)
//Function: 1. Load Image 
//          2. convert Bitmap to EImageC24 (with ptr)
//          3. Show EImageC24 Image to picturebox
//          4. Dispose or free memory after finished using the memory, to solve memory leak problem.
//Enviornment: VS 2015, eVision 2.2.3
//Caution: using eVision license, you need to run [Visual Studio] or [compiled exe] as administrator, if not it will not able to get the grant from license dongle.

using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Forms;
using Euresys.Open_eVision_2_3;
using System.Runtime.InteropServices;

namespace eVision_ShowImage
{
    public partial class Form1 : Form
    {
        //=================================================================================
        #region global variable
        Bitmap bitmap = null; //原Bitmap影像
        BitmapData bmpData = null; //BitmapData
        EImageC24 EC24Image1 = new EImageC24(); //eVision的彩色圖像物件
        float ScalingRatio = 0; //Picturebox與原始影像大小的縮放比例
        IntPtr bitmap_ptr = IntPtr.Zero; //Bitmap的記憶體指標，供影像記憶體轉換與清除使用
        #endregion global variable

        //=================================================================================
        #region form event
        public Form1()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 讀圖片檔
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_load_Click(object sender, EventArgs e)
        {
            //使用者選取Bitmap檔案
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                button_show.Enabled = true;
            }
        }
        
        /// <summary>
        /// 點擊後執行圖像轉換與顯示函式
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_show_Click(object sender, EventArgs e)
        {
            //讀取檔案的Bitmap (一般這一行也可以改成從相機的影像進行讀取）
            Bitmap bitmap_source = (Bitmap)Image.FromFile(openFileDialog1.FileName);
            //當bitmap尚未創建記憶體空間時直接使用Clone複製來源影像
            if (bitmap == null)
                bitmap = (Bitmap)bitmap_source.Clone(); //複製完整的記憶體空間
                                                        //指派影像
            bitmap = bitmap_source;

            //防止來源影像為空則不處理
            if (bitmap == null)
                return;

            //Bitmap影像轉換eVision格式影像
            bitmap_ptr = ConvertToIntPtr(bitmap);
            //Bitmap轉EImageC24格式
            EC24Image1 = BitmapToEImageC24(ref bitmap);
            //顯示影像於Picturebox
            ShowImage(EC24Image1, pictureBox_image);

            //因Bitmap容易造成記憶體外洩，因此將資料從記憶體中Dispose，並且進行GC
            bitmap.Dispose();
            //清除影像記憶體
            Marshal.FreeHGlobal(bitmap_ptr);

            //Garbage Collect需要耗費時間，建議在real-time的應用中不要使用
            //System.GC.Collect();
            //System.GC.WaitForPendingFinalizers();
        }

        /// <summary>
        /// 點擊後開啟timer，並且自動執行圖像轉換與顯示函式
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void checkBox_autoLoad_CheckedChanged(object sender, EventArgs e)
        {
            timer1.Enabled = checkBox_autoLoad.Checked;
        }
        
        /// <summary>
        /// 自動執行圖像轉換與顯示
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timer1_Tick(object sender, EventArgs e)
        {
            button_show_Click(null, null);
        }
        #endregion form event

        //=================================================================================
        #region customize functions
        /// <summary>
        /// Bitmap影像轉換eVision格式影像
        /// </summary>
        /// <param name="bitmap">來源Bitmap影像</param>
        /// <returns>轉換後的EImageC24影像</returns>
        private EImageC24 BitmapToEImageC24(ref Bitmap bitmap)
        {
            EImageC24 EC24Image1 = null;
            try
            {
                EC24Image1 = new EImageC24();

                Rectangle rect = new Rectangle(0, 0, bitmap.Width, bitmap.Height);
                bmpData =
                    bitmap.LockBits(rect, System.Drawing.Imaging.ImageLockMode.ReadWrite,
                    bitmap.PixelFormat);

                EC24Image1.SetImagePtr(bitmap.Width, bitmap.Height, bmpData.Scan0);
                bitmap.UnlockBits(bmpData);

            }
            catch (EException e)//EException為eVision的例外處理
            {
                Console.WriteLine(e.ToString());
            }
            return EC24Image1;
        }

        /// <summary>
        /// Bitmap 轉 IntPtr
        /// 從原始圖Bitmap創建新的記憶體，再回傳該記憶體的IntPtr
        /// </summary>
        /// <param name="Bmp">Bitmap 原始圖</param>
        /// <returns>圖像記憶體指標</returns>
        private IntPtr ConvertToIntPtr(Bitmap Bmp)
        {
            BitmapData bitmapData = Bmp.LockBits(new Rectangle(0, 0, Bmp.Width, Bmp.Height), ImageLockMode.ReadWrite, Bmp.PixelFormat);
            byte[] data = new byte[bitmapData.Stride * bitmapData.Height];
            Marshal.Copy(bitmapData.Scan0, data, 0, data.Length);
            IntPtr Ptr = Marshal.AllocHGlobal(data.Length);
            Marshal.Copy(data, 0, Ptr, data.Length);
            Bmp.UnlockBits(bitmapData);
            return Ptr;
        }

        /// <summary>
        /// 顯示影像於Picturebox
        /// </summary>
        /// <param name="img">顯示的eVision影像</param>
        /// <param name="pb">目標Picturebox</param>
        private void ShowImage(EImageC24 img, PictureBox pb)
        {
            try
            {
                Bitmap bmp;
                bmp = new Bitmap(pb.Width, pb.Height);

                //計算Picturebox與顯示影像的比例，以便將影像縮放並且完整呈現到picturebox上。
                float PictureBoxSizeRatio = (float)pb.Width / pb.Height;
                float ImageSizeRatio = (float)img.Width / img.Height;
                if (ImageSizeRatio > PictureBoxSizeRatio)
                    ScalingRatio = (float)pb.Width / img.Width;
                else
                    ScalingRatio = (float)pb.Height / img.Height;

                //委派
                if (pb.InvokeRequired)
                {
                    pb.Invoke(new MethodInvoker(delegate() {
                        img.Draw(Graphics.FromImage(bmp), ScalingRatio);
                        Bitmap bmp_temp = (Bitmap)pb.Image;
                        pb.Image = bmp; //顯示新的圖片
                        if (bmp_temp != null)
                            bmp_temp.Dispose();//清除picturebox前一張圖片的記憶體
                    }));
                }
                else
                {
                    //先將EImageC24畫在bitmap模式的圖檔
                    img.Draw(Graphics.FromImage(bmp), ScalingRatio);
                    //再由Picturebox取得bitmap
                    Bitmap bmp_temp = (Bitmap)pb.Image;
                    pb.Image = bmp;//顯示新的圖片
                    if (bmp_temp != null)
                        bmp_temp.Dispose(); //清除picturebox前一張圖片的記憶體
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }
        #endregion customize functions


    }
}
