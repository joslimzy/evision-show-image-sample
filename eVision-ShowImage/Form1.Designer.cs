﻿namespace eVision_ShowImage
{
    partial class Form1
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置 Managed 資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器
        /// 修改這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pictureBox_image = new System.Windows.Forms.PictureBox();
            this.button_load = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.button_show = new System.Windows.Forms.Button();
            this.checkBox_autoLoad = new System.Windows.Forms.CheckBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_image)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox_image
            // 
            this.pictureBox_image.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.pictureBox_image.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox_image.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox_image.Location = new System.Drawing.Point(0, 58);
            this.pictureBox_image.Name = "pictureBox_image";
            this.pictureBox_image.Size = new System.Drawing.Size(681, 507);
            this.pictureBox_image.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox_image.TabIndex = 0;
            this.pictureBox_image.TabStop = false;
            // 
            // button_load
            // 
            this.button_load.Location = new System.Drawing.Point(7, 8);
            this.button_load.Name = "button_load";
            this.button_load.Size = new System.Drawing.Size(86, 44);
            this.button_load.TabIndex = 1;
            this.button_load.Text = "Open file";
            this.button_load.UseVisualStyleBackColor = true;
            this.button_load.Click += new System.EventHandler(this.button_load_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // button_show
            // 
            this.button_show.Enabled = false;
            this.button_show.Location = new System.Drawing.Point(99, 8);
            this.button_show.Name = "button_show";
            this.button_show.Size = new System.Drawing.Size(86, 44);
            this.button_show.TabIndex = 2;
            this.button_show.Text = "Load and show";
            this.button_show.UseVisualStyleBackColor = true;
            this.button_show.Click += new System.EventHandler(this.button_show_Click);
            // 
            // checkBox_autoLoad
            // 
            this.checkBox_autoLoad.AutoSize = true;
            this.checkBox_autoLoad.Location = new System.Drawing.Point(201, 23);
            this.checkBox_autoLoad.Name = "checkBox_autoLoad";
            this.checkBox_autoLoad.Size = new System.Drawing.Size(47, 16);
            this.checkBox_autoLoad.TabIndex = 3;
            this.checkBox_autoLoad.Text = "Auto";
            this.checkBox_autoLoad.UseVisualStyleBackColor = true;
            this.checkBox_autoLoad.CheckedChanged += new System.EventHandler(this.checkBox_autoLoad_CheckedChanged);
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(681, 565);
            this.Controls.Add(this.checkBox_autoLoad);
            this.Controls.Add(this.button_show);
            this.Controls.Add(this.button_load);
            this.Controls.Add(this.pictureBox_image);
            this.Name = "Form1";
            this.Text = "eVision - Load Image and show";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_image)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox_image;
        private System.Windows.Forms.Button button_load;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button button_show;
        private System.Windows.Forms.CheckBox checkBox_autoLoad;
        private System.Windows.Forms.Timer timer1;
    }
}

